

.. index::
   ! Fédération Anarchiste Grenoble


.. _fagrenoble:

================================================================
Fédération Anarchiste de Grenoble
================================================================


.. sidebar:: FA Grenoble

    :Date: |today|
    :AuteurE()s: **patbakou AT pm.me**

    - https://gfagrenoble.frama.io/fagrenoble/index.html
    - https://gassr38.frama.io/assr38/
    - https://monde-libertaire.fr/
    - https://www.federation-anarchiste.org/
    - http://ecoutez.radio-libertaire.org/radiolib.m3u
    - https://fagrenobleblog.wordpress.com/
    - https://www.federation-anarchiste.org/?g=FA_Groupes
    - :ref:`genindex`
    - :ref:`search`



.. toctree::
   :maxdepth: 4

   principes_base/principes_base
   congres_FA/congres_FA
   groupes_fa/groupes_fa
   livres/livres
   outils/outils
   militantes/militantes
   luttes/luttes
   internationale_ifa/internationale_ifa
   communiques/communiques
