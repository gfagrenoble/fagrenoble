
.. index::
   pair: FA; Italie 
   
   
.. _fa_italie:
   
===================================================================================
Federazione Anarchica Italiana (FAIt) 
===================================================================================

.. seealso::

   - https://www.federation-anarchiste.org/?g=IFA_federations
   - http://www.umanitanova.org



Federazione Anarchica Italiana (FAIt)
Contact : 	crint@federazioneanarchica.org
Site web : 	http://www.federazioneanarchica.org
	
La Fédération Anarchiste Italienne a été fondée en 1945 et qui continue à se 
référer au programme de l‘Union Anarchiste Italienne de 1920.

La FAI s‘occupe de la rédaction et de l‘administration de l‘hebdomadaire 
anarchiste « Umanità Nova », qui tire à 3100 exemplaires et possède son 
propre site internet, a sa maison d‘édition munie d‘un ample catalogue 
de publications, a ses propres archives historiques, et propose et 
participe à de nombreux rendez-vous nationaux.

Dans les dernières années, l‘activité locale a été mise au cœur de 
l‘engagement de la Fédération.

Les groupes et les individuels de la Fédération sont actifs dans de nombreuses 
activités politiques, sociales, culturelles.

Pratiquement tous les groupes de la Fédération gèrent des locaux publics.

Quelques groupes rédigent des journaux locaux et ont des sites internet et des 
blogs. Beaucoup d‘anarchistes fédérés sont également investis de mandats 
syndicaux dans les organisations de base, et participent et promeuvent des 
comités de lutte sur les thèmes du nucléaire, des grands chantiers, de 
l‘écologie, de la lutte contre le racisme et anti-militariste, des universités 
libertaires, du mouvement No-Tav, des mouvements contre les expulsions et pour 
la réappropriation des espaces abandonnés.

Commission de Correspondance de la Fédération Anarchiste Italienne.

Umanita Nova : http://www.umanitanova.org
