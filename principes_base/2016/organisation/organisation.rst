
.. index::
   pair: 2016; Organisation 
   
   
.. _organisation_2016:
   
===============
Organisation
===============


.. contents::
   :depth: 3
   

Le Comité de relations
=======================

Un comité de coordination existe dans le but de faire connaître les informations, 
suggestions, propositions pouvant émaner d’un individu ou d’un groupe, sans 
autre droit, pour les camarades composant ce comité, que celui de n’importe 
quel autre militant de présenter propositions, suggestions, informations.

La seule dérogation à cela peut être constituée par des initiatives touchant 
l’adhésion à un congrès, cartel, comité (exemples : congrès anarchiste 
international, Forces libres de la Paix).

Il va de soi que, dans ces divers organismes, le fond même de notre idéologie 
ne doit pas être remis en cause et que notre présence ne doit viser qu’à des 
buts précis : opposition à la guerre, efforts pour arracher des militant-e-s 
à la mort, protestations contre une agression contre le peuple.

Le Comité de relations élargi
==============================


Il se réunit sur convocation du secrétaire général au moins trois fois par an. 
L’initiative peut venir du secrétaire général ou d’un comité de relations. 
L’ordre du jour est élaboré en fonction des demandes des groupes, liaisons, 
individuel-les, ou commissions. 
Ces demandes touchent tous les sujets concernant la vie de l’organisation. 
Le Comité de relations élargi est décisionnaire quant à la mise en place des 
campagnes fédérales. Qu’une campagne soit déclarée en congrès ou par un 
Comité de Relations Elargi, présentation des travaux doit être faite en 
comité de relations élargi et/ou dans le bulletin intérieur.

Secrétaire général
====================

Son rôle est de répartir le courrier et les responsabilités entre les 
différents membres, de veiller à la bonne entente au sein de ce comité, 
d’assurer la tenue du congrès selon le vœu du précédent, par des rapports 
en temps voulu avec les camarades qui en sont chargés.

Secrétaire aux Relations intérieures
======================================

Son rôle est d’assurer la correspondance avec les groupes, de les tenir au 
courant des lettres des sympathisants de leurs régions désireux de se joindre 
à nous, de diffuser les suggestions intéressantes, qu’elles émanent d’une région, 
d’un groupe ou d’une individualité, de renseigner le mouvement dans son entier 
des possibilités d’actions dont il dispose : noms de propagandistes par la 
parole, leurs sujets de conférence, leur possibilité de déplacement, d’assurer 
la propagande par des campagnes dont les thèmes peuvent être suggérés par 
d’autres militants, comme ils peuvent émaner de lui (les groupes en désaccord 
avec l’efficacité de celle-ci ont toujours la possibilité de ne pas y participer), 
de développer partout où il le peut la Fédération anarchiste par la création 
de nouveaux groupes, de présenter le bilan, lors de chaque congrès, de la 
progression, de la stagnation ou du recul de notre organisation en raisons 
des rapports des groupes.

Le secrétaire aux RI, lors de la constitution d’un groupe, prendra un contact 
direct avec celui-ci, soit en l’établissant lui-même, soit en déléguant, pour 
le représenter et lui en rendre compte, un camarade connu, habitant un lieu 
géographique proche du nouveau groupe.

Ces contacts, outre leur caractère humain, constitueront par le dialogue 
engagé une mesure de probité intellectuelle, tant de la part du groupe 
postulant vis-à-vis de la Fédération anarchiste, que de la Fédération 
anarchiste vis-à-vis du groupe postulant.

Secrétaire à la trésorerie
============================

Le trésorier perçoit directement des groupes ou individualités appartenant à 
la Fédération anarchiste une cotisation annuelle minimum (somme fixée 
volontairement peu élevée pour permettre à tous d’y souscrire et généralement 
la dépasser).

Dans le respect de l’autonomie des groupes, le trésorier n’a pas à connaître 
les noms et adresses des adhérents de chaque groupe, mais simplement leur 
nombre, confiance étant faite au trésorier du groupe qui le déclare.

Le trésorier tient au courant le secrétaire du Bulletin intérieur de la 
liste des adhérents (groupes ou individualités) à qui adresser ledit bulletin 
(réservé aux seuls membres de la Fédération anarchiste). La trésorerie permet 
d’assurer les frais de correspondance, les frais de déplacements des membres 
du comité au congrès, l’adhésion à des cartels, d’aider éventuellement à la 
sortie d’une affiche, à la propagation d’une campagne ou au soutien de 
notre journal.

Administration
================

Les administrateurs sont nommés par le congrès. Leur rôle est de veiller à 
la parution régulière du journal et à la bonne marche des œuvres du mouvement : 
Monde libertaire, librairie, etc…

Comme tous les autres responsables nommés par le congrès, ils peuvent 
s’entourer, pour les aider dans leur tâche, d’un ou plusieurs militant-e-s 
appointé-e-s ou non.

Comités de rédaction
======================

Les comités de rédaction ont pour fonction d'assurer la rédaction des différentes 
publications périodiques de la Fédération.

La création d'un comité de rédaction spécifique à une publication, tout 
comme sa suppression, est décidée en congrès.

Tous les articles soumis à l'appréciation d'un comité de rédaction, sauf 
opposition de principe motivée de l’un de ses membres, sont, soit publiés, 
soit transmis aux autres comités de rédaction si leur publication semble plus 
opportune dans un média différent.

Un article refusé est renvoyé à son auteur avec les raisons du refus.

Les membres de chaque Comité de rédaction sont nommées et révocables 
par le congrès.

Les membres d'un comité de rédaction peuvent coopter en cours de mandat, 
à l’unanimité, un ou plusieurs camarades en cas de défection d’un ou 
plusieurs de leurs membres.

Les différents comités de rédaction s'organisent entre eux pour assurer 
la complémentarité stratégique des différentes éditions.

Radio libertaire
==================

Le secrétariat de Radio libertaire participe comme les autres secrétariats au 
Comité de relations.
Quatre postes sont établis pour assurer le bon fonctionnement de la station :

- Le secrétaire à la programmation : il est responsable de la grille, de 
  l’agencement des programmes, de leur parution dans le Monde libertaire 
  et de la qualité des émissions. Il se tient en relation avec les différents 
  secrétariats Fédération anarchiste pour assurer le passage des informations 
  et des communiqués sur Radio libertaire
- Le secrétaire aux finances : il est chargé de gérer le budget de la radio et 
  de veiller à l’autofinancement de Radio libertaire
- Le secrétaire à la technique et à l’entretien : il est responsable de la 
  maintenance du matériel et du studio, présente des prévisions d’investissement 
  au secrétaire aux finances
- Le secrétaire de l’association : il s’occupe du fonctionnement de 
  l’association "Diffusion des moyens de communication", garante de Radio 
  libertaire au niveau légal.


Publico
========

Le secrétariat de Publico participe comme les autres secrétariats au 
Comité de relations.

Deux postes sont établis pour assurer le bon fonctionnement de la librairie.

Le secrétaire en charge de l'animation 
---------------------------------------

- est responsable des activités publiques et économiques organisées dans la 
  librairie ainsi que des activités de soutien à la librairie ;
- assure la gestion des outils de propagande et d'information propres 
  à la librairie ;
- veille à la diversité des publications proposées à la vente.

L'administrateur 
-----------------

- est responsable de la tenue des comptes de Publico ;
- assure la gestion administrative et financière de la librairie ;
- assure la gestion administrative et financière des locaux dont 
  Publico à la responsabilité.


Secrétaire à l’Histoire et aux archives
=========================================

Sa fonction est de rassembler, d’archiver différents documents (affiches, 
brochures, tracts, livres, etc.) ayant trait au mouvement anarchiste en général 
et à la Fédération anarchiste en particulier, dans le but de sauvegarder 
notre mémoire militante. Pour assurer ces fonctions, le secrétaire peut 
s’entourer de militants.

Secrétaire aux Editions
=========================

Son rôle est de contribuer à l’édition ou à la réédition de textes 
particulièrement importants pour l’anarchisme et sa propagande. 
Il doit assurer la promotion et la diffusion des productions des éditions 
du Monde Libertaire.

Fonctionnement du Comité de relations
======================================


La lecture de ce qui précède indique assez clairement l’interpénétration des 
différents postes, et notamment la liaison qui ne peut cesser d’exister entre 
les secrétaires aux Relations intérieures et aux Relations extérieures, cela 
en contact avec le secrétaire à la trésorerie, qui règle en fonction de ses 
possibilités l’envergure et la périodicité des actions à entreprendre.

Ce comité se réunit de façon régulière et sur convocation du secrétaire général.

Chaque secrétaire est responsable de son poste et, par conséquent, est seul à 
décider des mesures à prendre en ce qui le concerne.

Toutefois, il ne peut le faire, tant pour des raisons pratiques que morales, 
sans en avoir débattu avec tous et sans avoir tenu compte des objections, 
oppositions qui peuvent lui avoir été apportées. Ainsi, si le travail 
s’accomplit en équipe, les décisions en dernier ressort sont prises par les 
ressortissants à chacun des postes, seuls responsables devant le congrès pour 
ce qui les concerne.


Chaque secrétaire peut s’entourer d’une commission de son choix pour l’aider 
dans sa tache et dont il prend l’entière responsabilité.

Le Bulletin intérieur
=======================

Le Bulletin intérieur est indépendant du Comité de relations, et pour son 
contenu, et pour son financement. Le CR ne peut y exercer un droit quelconque 
de censure ou d’interdit. Il est ouvert à tous sans que joue la moindre censure 
de la part de ceux qui en ont la charge.

Tous les articles y ont leur place, sauf ceux qui pourraient avoir un 
caractère de calomnie.

Tout article refusé par le Comité de rédaction du Monde libertaire peut 
paraître avec cette mention dans le BI..

Enfin, le Comité de relations y a recours lui-même pour ses communiqués aux 
groupes, ses actions, ses rapports d’activité lors de chaque congrès.

Le chapeautage de textes (groupes et individuels) ne doit pas exister dans le BI.

Le groupe gestionnaire du BI doit attendre le numéro suivant pour donner sa 
position, au même titre que les autres groupes.

Les abonnements au BI
========================

Lorsque le groupe gestionnaire du BI reçoit une demande d’abonnement au BI, il 
demandera la conformité de l’appartenance à l’organisation du demandeur à la 
trésorerie. Les mêmes contrôles s’effectueront pour les adhésions au travers 
du groupe, afin que les groupes ne reçoivent pas un nombre supérieur de BI au 
nombre adhérents, plus un pour les archives. 
En cas de démissions, la trésorerie informe les gestionnaires des démissions 
d’individuels ou de groupes. 
Les secrétaires de groupes signaleront directement les démissions dans leurs 
groupes pour rectifier le nombre de BI à recevoir.

La parution du BI sera mensuelle (sauf s’il n’y a pas d’articles).

Sauf impossibilité, son siège est fixé en province, tant pour décongestionner 
la capitale que pour permettre à cet organe une parfaite autonomie.

En cas de défaillance du groupe responsable, le Secrétaire général peut en 
confier provisoirement la tâche à un autre groupe.

Le congrès
=============

Le congrès, ouvert aux seuls membres de la Fédération anarchiste (sauf invitation 
du Comité de relations), organisé par le groupe proposé et désigné par le précédent 
congrès, en liaison avec le Secrétaire général, a pour objet :

- De faire le point de la situation morale et financière de la Fédération 
  anarchiste et de ses œuvres
- De débattre des motions, propositions, études, présentées par les groupes, 
  individuels ou organisme de la Fédération anarchiste, et d’en tirer les grandes 
  lignes des campagnes à mener au cours de l’année, ainsi que les décisions 
  nécessaires à la bonne marche de la Fédération.


L’adhésion
===========

L’adhésion à la Fédération Anarchiste repose sur deux facteurs :

- l’un matériel : le règlement des cotisations
- l’autre moral : l’acceptation sans réserve des principes énoncés par la 
  présente déclaration.


Il en résulte que leur remise en cause publique consiste par là même la rupture 
de l’auteur avec la Fédération anarchiste.

Les principes de base ne peuvent être complétés ou modifiés (après proposition 
de textes soumis quatre mois avant le congrès) que par l’unanimité de celui-ci, 
mais sans que puissent être remis en cause nos concepts indiqués dans les 
préambules : autonomie des groupes et pluralité des tendances.

Tout membre de la Fédération Anarchiste a le droit et non l’obligation d’adhérer 
à des organisations culturelles, philosophiques, syndicales, pacifistes, de 
loisirs de son choix, dans le mesure où ces adhésions sont compatibles avec 
l’anarchie.

Cependant, toute liberté est laissée sur le plan de l’organisation du groupe 
de mener avec ces mouvements des actions communes.

Quiconque souscrit à cette déclaration peut adhérer à la Fédération anarchiste, 
soit par le canal d’un groupe, soit individuellement (en raison de son 
éloignement de tout groupe ou simplement parce qu’il désire rester isolé).
