

.. index::
   pair: Congrès ; 2017



.. _congres_FA_2017:

=======================================================================================
Congrès de la Fédération Anarchiste les 3, 4 et 5 juin 2017 Laon et Merlieux (Aisne)
=======================================================================================



.. toctree::
   :maxdepth: 3
   
   motions/motions
