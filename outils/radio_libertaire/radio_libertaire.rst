

.. index::
   pair: Radios Libertaire ; Outil

.. _radio_libertaire:

===================
Radio Libertaire
===================

.. seealso::

   - http://ecoutez.radio-libertaire.org/radiolib.m3u
   - https://www.radio-libertaire.net/
   - https://www.federation-anarchiste.org/rl/prog/
   

Les syndicats de la CNT animent une émission sur Radio libertaire les mardis 
de 20h30 à 22h30

- 1er mardi : CNT-RP
- 2e mardi : CNT éducation
- 4e mardi : Sévices publics (Énergie)

La CNT 94 anime le 5e dimanche l’émission Micro-Ondes 94 de 15h30 à 17 heures
