
.. index::
   ! Errico Malatesta


.. _errico_malatesta:

=====================================
Errico Malatesta
=====================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Errico_Malatesta


.. contents::
   :depth: 3

.. figure:: Malatesta.png
   :align: center
   

Biographie
===========


Errico Malatesta (né le 14 décembre 1853 à Santa Maria Capua Vetere, dans la 
province de Caserte, en Campanie, Italie - mort le 22 juillet 1932) est un 
intellectuel, écrivain, propagandiste et révolutionnaire anarchiste italien.

Étudiant en médecine à **Naples** et déjà républicain, il adhère à l’anarchisme à 
**la suite de la Commune de Paris (1871)**. 

Au congrès de Berne de l'Association internationale des travailleurs (1876), 
il préconise la « propagande par le fait » comme moyen d'action. 
Il est condamné à seize mois de prison pour sa participation à l'insurrection 
de Bénévent (1877). Rentré en Italie en 1914, il est considéré comme le principal 
responsable de la « Semaine rouge » d’Ancône (7-14 juin 1914)1. Il occupe une 
place importante dans le mouvement libertaire international du fait de sa 
capacité critique et pratique.

Il est avec Pierre Kropotkine l'un des principaux théoriciens du communisme 
libertaire et élabore le concept de « gradualisme révolutionnaire » qui postule 
que l'anarchie ne peut être réalisée que par un processus cumulatif d'étapes 
additionnées.

   
