

.. index::
   pair: Livre ; Albert Camus, Elisée Reclus et l’Algérie
   pair: Auteur ; Philippe Pelletier
   pair: Camus ; Reclus



.. _camus_reclus:

=======================================================================================================
Albert Camus, Elisée Reclus et l’Algérie de Philippe Pelletier
=======================================================================================================

.. seealso::

   - http://www.lecavalierbleu.com/livre/albert-camus-elisee-reclus-et-lalgerie/
   - :ref:`philippe_pelletier`
   

.. contents::
   :depth: 3


.. figure:: Camus-Reclus.jpg
   :align: center
   

Présentation
======================

Élisée Reclus, Albert Camus, l’Algérie : deux personnages, une contrée, une 
convergence finalement évidente. Malgré le demi-siècle qui les sépare, malgré 
les différences de métier, de contexte ou de caractère, Reclus et Camus partagent 
de nombreux points communs : honnêteté intellectuelle, exigence éthique, 
convictions libertaires et passion pour l’Algérie. 

Véritable fil noir et rouge, qui passe d’abord par un attachement familial, 
ce pays traduit en effet leur sentiment d’être des « indigènes de l’univers ». 
Leur dénonciation du colonialisme, exempte de nationalisme, fut mal comprise. 

Leur alternative autogestionnaire et fédéraliste aurait permis des issues 
moins douloureuses.

Commentaires
==============

.. seealso:: https://journals.openedition.org/lectures/19968




