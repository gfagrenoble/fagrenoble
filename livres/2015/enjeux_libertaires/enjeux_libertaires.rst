

.. index::
   pair: Livre ; Enjeux libertaires pour le XXIe siecle

.. _enjeux_libertaies:

==========================================================
Enjeux libertaires pour le XXIe siecle de Philippe Corcuff 
==========================================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Philippe_Corcuff
   - :ref:`philippe_corcuff`
   - http://www.grand-angle-libertaire.net/enjeux-libertaires-pour-le-xxie-siecle-par-un-anarchiste-neophyte-philippe-corcuff/

.. figure:: Philippe_Corcuff_Enjeux-libertaires-pour-le-XXIe-siecle_2015_11_01.jpg
   :align: center
   
   
